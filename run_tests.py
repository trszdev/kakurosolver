from pip import main as pip_main
pip_main(['install', '-r', 'requirements.txt'])

from pytest import main as pytest_main
pytest_main(['tests.py'])