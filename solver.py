from graph import Graph, FormatError
from sys import stdin, stderr


if __name__ == '__main__':
    try:
        graph = Graph.from_lines(stdin).solve()
        if graph is None:
            print('Kakuro can\'t be solved', file=stderr)
        else:
            for line in graph.to_lines():
                print(line)
    except FormatError as error:
        print(f'Input file has invalid format. {error}.', file=stderr)
