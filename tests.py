from graph import Graph, FormatError
from assertpy import assert_that
from parameterized import parameterized


def read_graph(filename):
    with open('./cases/' + filename) as file:
        lines = tuple(file)
        return Graph.from_lines(lines)


@parameterized.expand([
    ('3x3.1.solved.txt', True),
    ('3x3.1.txt', False),
    ('4x4.1.txt', False),
    ('4x4.1.solved.txt', True),
    ('3x3.1.wrong.txt', False),
    ('9x8.1.txt', False),
    ('20x20.1.txt', False)
])
def test_is_solved(filename, was_solved):
    graph = read_graph(filename)
    assert_that(graph.is_solved()).is_equal_to(was_solved)


@parameterized.expand([
    '3x3.1.solved.txt',
    '3x3.1.txt',
    '4x4.1.txt',
    '4x4.1.solved.txt',
    '9x8.1.txt',
    #'20x20.1.txt',  # <-- complexity growth, see readme
    'endurance.txt',
    'endurance_fullrange.txt',
    '6x6.1.txt'
])
def test_solve(filename):
    graph = read_graph(filename).solve()
    assert_that(graph.is_solved()).is_true()


@parameterized.expand([
    '3x3.1.txt',
    '3x3.1.int.txt',
])
def test_after_intersection(filename):
    graph = read_graph(filename).after_intersection()
    assert_that(graph.is_solved()).is_true()


@parameterized.expand([f'invalid{x}.txt' for x in range(1, 9)])
def test_format_error(filename):
    assert_that(read_graph).raises(FormatError).when_called_with(filename)


@parameterized.expand([
    '3x3.1.nosolution.txt',
    '4x4.1.nosolution.txt',
])
def test_no_solution(filename):
    graph = read_graph(filename).solve()
    assert_that(graph).is_none()