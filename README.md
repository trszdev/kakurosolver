# KakuroSolver

Solves kakuro using backtracking and intersections

Requires python3.6+

## Brand New Kakuro Solver
This one solves kakuro of alphabet [-9 ... 9] (clues can be non-positive); if you want classic version, look for `normal-version` tag

So, this leads to complexity growth:
```python
from graph import _sums
lens = [len(v) for v in _sums.values()]
lens.sort()
print('AVG', sum(lens) // len(lens)) # normal: 3, new: 452
print('MIN', min(lens)) # 1
print('MAX', max(lens)) # normal: 12, new: 2934
print('MEDIAN', lens[len(lens)//2]) # normal: 3, new: 97
```

## Usage
```python
from graph import Graph

with open('20x20.1.txt') as file:
    solved = Graph.from_lines(tuple(file)).solve()
    print(solved.to_lines())
```

Sample files located in cases/

## Tests
```bash
python run_tests.py
```

That will also install test dependencies 

## Naming
![Naming](naming.png "Naming convention")