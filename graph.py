from typing import Optional
from graph_helper import *
from itertools import product


Lines = Tuple[str, ...]
_sums = get_all_sums()


class Graph(Object):
    def __init__(self, width: int, height: int, edges: Edges, vertices: Vertices):
        self.edges = edges
        self.vertices = vertices
        self.width = width
        self.height = height

    @staticmethod
    def from_lines(lines: Lines) -> 'Graph':
        rows = [x.split() for x in lines]
        width, height = get_dimensions(rows)
        kakuro = create_kakuro(width, height, [None, None])
        vertices, edges = [], []
        for y, row in enumerate(rows):
            for x, cell in enumerate(row):
                if is_stone_cell(cell):
                    vertices.extend(parse_stone_cell(cell, x, y, kakuro))
                else:
                    edges.append(parse_free_cell(cell, x, y, kakuro))
        vertices = tuple(vertices)
        check_vertices(vertices)
        return Graph(width, height, tuple(edges), vertices)

    def to_lines(self) -> Lines:
        kakuro, result = create_kakuro(self.width, self.height, ['XX', 'XX']), []
        for edge in self.edges:
            add_edge(edge, kakuro)
        for y in range(self.height):
            line = ['\\'.join(kakuro[y][x]) for x in range(self.width)]
            result.append(' '.join(line))
        return tuple(result)

    def solve(self) -> Optional['Graph']:
        stack = [self]
        while stack:
            graph = stack.pop().after_intersection()
            if graph is None:
                continue
            if graph.is_determined() and graph.is_solved():
                return graph
            possible_edges = filter(lambda x: len(x[1].numbers) > 1, enumerate(graph.edges))
            index, edge = min(possible_edges, key=lambda x: len(x[1].numbers))
            edges = graph.edges[:index] + graph.edges[index + 1:]
            for number in edge.numbers:
                edge_copy = Edge(edge.start, edge.end, frozenset([number]), edge.position)
                stack.append(Graph(self.width, self.height, edges + (edge_copy,), self.vertices))
        return None

    def is_determined(self) -> bool:
        return all(len(x.numbers) == 1 for x in self.edges)

    def is_solved(self) -> bool:
        if not self.is_determined():
            return False
        vertex_edges = traverse_edges(self.edges)
        for vertex, edge_indexes in vertex_edges.items():
            edges = list(self.edges[x] for x in edge_indexes)
            if len(set([x.numbers for x in edges])) != vertex.times or \
                            sum(sum(x.numbers) for x in edges) != vertex.clue:
                return False
        return True

    def after_intersection(self) -> Optional['Graph']:
        edges = list(self.edges)
        vertex_edges = traverse_edges(self.edges)
        changed = True
        while changed:
            changed = False
            for i, edge in enumerate(list(edges)):
                if not edge.numbers:
                    return None
                if len(edge.numbers) == 1:
                    for j in vertex_edges[edge.start].union(vertex_edges[edge.end]).difference([i]):
                        adj = edges[j]
                        nums = adj.numbers.difference(edge.numbers)
                        if nums != adj.numbers:
                            changed = True
                            edges[j] = Edge(adj.start, adj.end, frozenset(nums), adj.position)
                else:
                    s1s = _sums[edge.start.clue, edge.start.times]
                    s2s = _sums[edge.end.clue, edge.end.times]
                    nums = set()
                    for s1, s2 in product(s1s, s2s):
                        nums.update(edge.numbers.intersection(s1, s2))
                    if nums != edge.numbers:
                        changed = True
                        edges[i] = Edge(edge.start, edge.end, frozenset(nums), edge.position)
            for vertex, edge_indexes in vertex_edges.items():
                vedges = list(edges[x] for x in edge_indexes)
                sums = set(_sums[vertex.clue, vertex.times])
                for edge in vedges:
                    for s in set(sums):
                        if not s.intersection(edge.numbers):
                            sums.remove(s)
                for i, edge in zip(edge_indexes, vedges):
                    nums = set()
                    for s in sums:
                        nums.update(s.intersection(edge.numbers))
                    if nums != edge.numbers:
                        changed = True
                        edges[i] = Edge(edge.start, edge.end, frozenset(nums), edge.position)
        return Graph(self.width, self.height, tuple(edges), self.vertices)



