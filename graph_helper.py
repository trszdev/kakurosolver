from typing import List, Tuple, Iterable, Any, FrozenSet
from collections import namedtuple, defaultdict
from re import match
from copy import copy
from itertools import combinations

ListWithTwoItems = List[Any]
Kakuro = List[List[ListWithTwoItems]]
Point = namedtuple('Point', 'x y')


class Object:
    def __repr__(self):
        props = ', '.join(f'{k}={v}' for k, v in self.__dict__.items())
        return f'{self.__class__.__name__}({props})'

    def __hash__(self) -> int:
        return sum(hash(v) for v in self.__dict__.values())

    def __eq__(self, o: object) -> bool:
        return self.__dict__ == o.__dict__


class FormatError(Exception):
    def __init__(self, message: str = ''):
        self.message = message

    def __repr__(self) -> str:
        return self.message


class Vertex(Object):
    def __init__(self, clue: int, times: int, position: Point):
        self.clue = clue
        self.times = times
        self.position = position


class Edge(Object):
    def __init__(self, start: Vertex, end: Vertex,
                 numbers: FrozenSet[int], position: Point):
        self.start = start
        self.end = end
        self.numbers = numbers
        self.position = position


Edges = Tuple[Edge, ...]
Vertices = Tuple[Vertex, ...]


def check_vertices(vertices: Vertices) -> None:
    if not vertices:
        raise FormatError('No stone blocks found')
    for x in vertices:
        if not 0 < x.times < 20:
            raise FormatError('Bad stone block: invalid amount'
                              f' of free cells ({x.position.x}, {x.position.y})')


def get_dimensions(rows: List[List[str]]) -> Tuple[int, int]:
    height = len(rows)
    if not height:
        raise FormatError('Height can\'t be zero')
    width = len(rows[0])
    if any(len(x) != width for x in rows):
        raise FormatError('Inconsistent width')
    return width, height


def is_stone_cell(cell: str) -> bool:
    return '\\' in cell


def parse_stone_cell(cell: str, x: int, y: int, kakuro: Kakuro) -> Iterable[Vertex]:
    b, r = cell.split('\\')
    if match(r'^-?\d+$', r):
        r = int(r)
        if not -45 <= r <= 45:
            raise FormatError(f'Impossible clue - "{cell}" at ({x}, {y})')
        v = Vertex(r, 0, Point(x, y))
        width = len(kakuro[0])
        for i in range(x, width):
            kakuro[y][i][0] = v
        yield v
    if match(r'^-?\d+$', b):
        b = int(b)
        if not -45 <= b <= 45:
            raise FormatError(f'Impossible clue - "{cell}" at ({x}, {y})')
        v = Vertex(b, 0, Point(x, y))
        height = len(kakuro)
        for j in range(y, height):
            kakuro[j][x][1] = v
        yield v


def parse_free_cell(cell: str, x: int, y: int, kakuro: Kakuro) -> Edge:
    nums = frozenset(map(int, cell.split(','))) \
        if match(r'^(-?\d+,)*-?\d+$', cell) else frozenset(range(-9, 10))
    b, r = kakuro[y][x]
    if b is None and r is None:
        raise FormatError(f'Single cells aren\'t allowed ({x}, {y})')
    if b is None:
        b = r
        b.times -= 1
    if r is None:
        r = b
        r.times -= 1
    b.times += 1
    r.times += 1
    return Edge(r, b, nums, Point(x, y))


def add_edge(edge: Edge, kakuro: Kakuro):
    nums = list(map(str, edge.numbers))
    x, y = edge.position
    kakuro[y][x] = ['*' if len(nums) == 19 else ','.join(nums)]
    sx, sy = edge.start.position
    is_vertical = sx == edge.position.x
    kakuro[sy][sx][1 if is_vertical else 0] = str(edge.start.clue)
    ex, ey = edge.end.position
    if ex == sx and sy == ey:
        return
    is_vertical = ex == edge.position.x
    kakuro[ey][ex][1 if is_vertical else 0] = str(edge.end.clue)


def traverse_edges(edges: Edges):
    vertex_edges = defaultdict(set)
    for i, edge in enumerate(edges):
        vertex_edges[edge.start].add(i)
        vertex_edges[edge.end].add(i)
    return vertex_edges


def create_kakuro(width: int, height: int, elem: Any) -> Kakuro:
    return [[copy(elem) for _ in range(width)] for _ in range(height)]


def get_all_sums():
    result = defaultdict(list)
    for x in range(1, 20):
        for c in combinations(range(-9, 10), x):
            result[sum(c), x].append(frozenset(c))
    return result
